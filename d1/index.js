console.log("Hello javascript objects")


//Array is a collection of related data

const grades = [91, 92, 93, 94, 95];
const names = ["Joy", "natalia", "Bianca"];



//objects
	//collection of multiple values with different data type

const objGrades = {
	//property: value (key-value pairs)
	firstName: "Aaron",
	lastName: "Delos Santos",
	firstGrading: 91,
	subject: "English",
	teachers: ["Carlo", "Masahiro"],
	isActive: true,
	schools: {
		city: "Manila",
		country: "Philippines"
	},
	studentNames: [
		{
			name: "Adrian",
			batch: "152"

		},
		{
			name: "Nikko",
			batch: "152"
		}

	],
	description: function(){
		return `${this.subject}: ${this.firstGrading} of students 
			${this.studentNames.name} and ${this.studentNames.name}`
	}	
}

/*how do we access properties of an object
	//dot notation(.)
	bracket notation ([""])

syntax:

	objReference.propertyName
	objReference.["propertyName"]




*/

console.log(objGrades.firstGrading); //91
console.log(objGrades.subject);		//English
console.log(objGrades["isActive"]);
console.log(objGrades["teachers"]);
console.log(objGrades["firstName"]);

console.log(objGrades.description());

//schools, access country property

console.log(objGrades.schools.country);
console.log(objGrades["schools"]["city"]);


//In studentNames property, access the second element
console.log(objGrades["studentNames"][1]);
console.log(objGrades.studentNames[1].name); //Nikko
console.log(objGrades.studentNames[1]["batch"]); //batch 152

//add a new property in an object with the use use
//of dot notation & assignment operator
objGrades.semester = "first"

//console.log(objGrades);

//possible to delete a property in an object?
delete objGrades.semester;
console.log(objGrades);


//Mini-Activity

const studentGrades = [
    { studentId: 1, Q1: 89.3, Q2: 91.2, Q3: 93.3, Q4: 89.8 },
    { studentId: 2, Q1: 69.2, Q2: 71.3, Q3: 76.5, Q4: 81.9 },
    { studentId: 3, Q1: 95.7, Q2: 91.4, Q3: 90.7, Q4: 85.6 },
    { studentId: 4, Q1: 86.9, Q2: 74.5, Q3: 83.3, Q4: 86.1 },
    { studentId: 5, Q1: 70.9, Q2: 73.8, Q3: 80.2, Q4: 81.8 }
];

//average = studentId() = (q1 + q2 + q3 + q4)/4





//solution 1 using manual computation
//1st element
/*let ave1 = (studentGrades[0].q1 + studentGrades[0].q2 +
	studentGrades[0].q3 + studentGrades[0].q4)/4
console.log(ave1)
studentGrades[0].average = ave1
console.log(studentGrades)


let ave2 = (studentGrades[1].q1 + studentGrades[1].q2 +
	studentGrades[1].q3 + studentGrades[1].q4)/4
console.log(ave2)
studentGrades[1].average = ave2
console.log(studentGrades)
*/


//solution 2 for loop

for(let i = 0; i < studentGrades.length; i++){
	let ave = (studentGrades[i].q1 + studentGrades[i].q2 +studentGrades[i].q3 +
		studentGrades[i].q4) /4
	console.log(ave)

	studentGrades[i].average = parseFloat(ave.toFixed(1))
}

//solution 3 using forEach

/*studentGrades.forEach(function(element) {
	console.log(element)

	let ave = (element.q1 + element.q2 + element.q3 + element.q4)/4
	console.log(ave)

	element.average = parseFloat(ave.toFixed(1))
})*/

console.log(studentGrades)


function Pokemon(name,lvl,hp){
	this.name = name,
	this.level = lvl,
	this.health = hp * 2,
	this.attack =lvl,
	this.tackle =function(target){
		console.log(target)//object of charizard
		//change the statement to "Pikachu tackled Charizard"
		console.log(`${this.name} tackled ${target.name}`)

		//change the statement to "Charizard's health is now reduced to #"
		console.log(`${target.name}'s health is now reduced to ${
			target.health - this.attack}`)
		
		//using compound assignment operator
		//health - attack = health

		target.health -= this.attack

		if(target.health < 10){
			target.faint()
		}

		},
			
	
	this.faint = function(){
		console.log(`Pokemon fainted`)
	}
}

let pikachu = new Pokemon("Pikachu", 5, 50)
let charizard = new Pokemon("Charizard", 8, 40)



//new is invoking a template


let squirtle = new Pokemon("Squirtle", 5, 20)
let bulbasaur = new Pokemon("Bulbasaur", 8, 20)

console.log(squirtle)
console.log(bulbasaur)

console.log(squirtle.tackle(bulbasaur))
console.log(squirtle.tackle(bulbasaur))


let object1 = {
	property: "sample"
}


let array1 = ["sample", "sample1"];

console.log(typeof object1);
console.log(typeof array1);


//arrays are a special type of data object!!!!!!!!!!
//what to use to check the type of data

let object2 = {}

console.log({} === object2)

/* Any instance of an object is unique. Ho two objects are the same and thus we 
cannot use the === strict equality comparison on the objects themselves, however
we can compare their properties.*/

let object3 = {
	name: "sample2"
}

let object4 = {
	name: "sample2"
}

console.log(object3 === object4)
console.log(object3.name === object4.name)



function Dog(name,breed,age){
	/*refers to the object you will create w/ constructor*/
	//this.property = value
	this.name = name;
	this.breed = breed;
	this.age = age * 7;
	this.bark = function(){
		console.log("Woof!")
	}
}


/*new keyword to create a new object out of our constructor
if you don't use the new keyword, the object will not be create.*/

let dog1 = new Dog("Bolt","Corgi",5);
console.log(dog1);


/* 

Conventions when creating our constructor:

	To distiguish it with othewr functions we capitalize the name of the 
	constructor.










