const trainer = {
	Name:"Adrian",
	Age: 32,
	Pokemon: ["Tyranitar", "Dragonite", "Charizard", "Rayquaza"],
	Friends: {
		eastcoast: ["Zara", "Scott"],
		westcoast: ["Luigi", "Nora"]
	}
};

console.log (trainer["Name"]);
console.log (trainer ["Age"]);
console.log (trainer["Pokemon"]);
console.log (trainer["Friends"]);


trainer.talk = function () {
	console.log("Pikachu! I choose you!")
};

trainer.talk();

function Pokemon(name, lvl, hp){
	this.name = name,
	this.level = lvl,
	this.health = hp * 2,
	this.attack = lvl,
	this.tackle = function(target){
		console.log(`${this.name} tackled ${target.name}`)

		console.log(`${target.name}'s health is now reduced to ${target.health - this.attack}`)

		target.health -= this.attack

		if(target.health <= 0){
			target.faint()
		}

	},
	this.faint = function(){
		console.log(`Pokemon fainted`)
	}
}

let tyranitar = new Pokemon("Tyranitar", 12, 45)
let dragonite = new Pokemon("Dragonite", 10, 40)
let charizard = new Pokemon("Charizard", 9, 35)
let rayquaza = new Pokemon("Rayquaza", 13, 50)

console.log(tyranitar)
console.log(dragonite)
console.log(charizard)
console.log(rayquaza)

console.log(tyranitar.tackle(dragonite))
console.log(tyranitar.tackle(dragonite))
console.log(tyranitar.tackle(dragonite))
console.log(tyranitar.tackle(dragonite))
console.log(tyranitar.tackle(dragonite))
console.log(tyranitar.tackle(dragonite))
console.log(tyranitar.tackle(dragonite))

console.log(tyranitar.tackle(charizard))
console.log(tyranitar.tackle(charizard))
console.log(tyranitar.tackle(charizard))
console.log(tyranitar.tackle(charizard))
console.log(tyranitar.tackle(charizard))
console.log(tyranitar.tackle(charizard))

console.log(tyranitar.tackle(rayquaza))
console.log(tyranitar.tackle(rayquaza))
console.log(tyranitar.tackle(rayquaza))
console.log(tyranitar.tackle(rayquaza))
console.log(tyranitar.tackle(rayquaza))
console.log(tyranitar.tackle(rayquaza))
console.log(tyranitar.tackle(rayquaza))
console.log(tyranitar.tackle(rayquaza))
console.log(tyranitar.tackle(rayquaza))

console.log(rayquaza.tackle(tyranitar))
console.log(rayquaza.tackle(tyranitar))
console.log(rayquaza.tackle(tyranitar))
console.log(rayquaza.tackle(tyranitar))
console.log(rayquaza.tackle(tyranitar))
console.log(rayquaza.tackle(tyranitar))
console.log(rayquaza.tackle(tyranitar))




